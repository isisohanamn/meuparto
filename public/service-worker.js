var cacheName = "sgtoilet-cache-" + Date.now();
const urlsToCache = [
  "/",
  "/index.html",
  '/bundle.css',
  '/global.css',
  '/bundle.js'
];

self.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(cacheName)
      .then((cache) =>  cache.addAll(urlsToCache))
  );
});

self.addEventListener("activate", e => {
  e.waitUntil(
    (async function() {
    let cacheNames = await caches.keys();
    cacheNames = await Promise.all(
        cacheNames
          .filter(thisCacheName => thisCacheName !== cacheName)
          .map((thisCacheName) => caches.delete(thisCacheName))
      )
      return cacheNames;
    })()
  );
});
self.addEventListener("fetch", e => {
  e.respondWith(
    (async function() {
      return await caches.match(e.request) || fetch(e.request);
    })()
  );
});
