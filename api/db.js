require('dotenv').config()

const MongoClient = require('mongodb').MongoClient;

const {
  DB_NAME,
  DB_USER,
  DB_PASS
} = process.env;

const uri = `mongodb+srv://${DB_USER}:${DB_PASS}@cluster0.2ifeb.gcp.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`;

const client = new MongoClient(uri, { useNewUrlParser: true });

client.connect(err => {
  if ( err ) {
    client.close();
  }
});


const collection = client.db("parto").collection("relatos");

collection.find({});