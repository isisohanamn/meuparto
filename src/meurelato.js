const test =() => {
    showModal = true;
    editor
      .save()
      .then((outputData) => {
        console.log(outputData);
        output = outputData.blocks;
        console
        href = href +output.map(block => {
          if (  block.type === 'header')  {
             return "✏ *"+block.data.text+"* ✍" 
          }  else { 
            const { text } = block.data;
            const doutora = [ 'ginecologista', 'obstetra', 'doutora' ];
            const insertMaternidade = (txt) =>  txt.split(" ").map(word => (word === 'maternidade') ? word + " 🏩" : word).join(' ');
            const insertDoutora = (txt) =>  txt.split(" ").map(word => (doutora.indexOf(word) > -1) ? " ⚕" + word : word).join(' ');
            const insertAmamentacao = (txt) =>  txt.split(" ").map(word => (word === 'amamentacao') ? " 🤱" + word : word).join(' ');
            const insertGravidez = (txt) =>  txt.split(" ").map(word => (word === 'gravidez') ? " 🤰" + word : word).join(' ');
            const insertBebe = (txt) =>  txt.split(" ").map(word => (word === 'bebe') ? " 👶" + word : word).join(' ');
            const dataCalendar = (txt) =>  txt.split(" ").map(word => /\d+\/\d+$/.test(word) ? " 🗓" + word : word).join(' ');
            const hour = (txt) =>  txt.split(" ").map(word => /\d+:\d+$/.test(word) ? " 🕐" + word : word).join(' ');
            const porEmotion = compose(
              insertMaternidade,
              insertDoutora,
              insertAmamentacao,
              insertGravidez,
              insertBebe,
              dataCalendar,
              hour
            );
             const createTimeline= (txt) =>{
           const arrnew = []
            let indice = 1;
            
             const qtdCaracterTela = 25;
            arrnew.push('||♡')
            
            arrnew.push('|| '+'_'.repeat(qtdCaracterTela-3))
            
            
            
            txt.split(/\s/).reduce((acc, palavra, idx) => {
              const tamanhoDaPalavra = palavra.length;
              const tamanhoDaPalavraAcumulada = acc.length;
            
              const atingiuLimite = tamanhoDaPalavra + tamanhoDaPalavraAcumulada>= qtdCaracterTela;
            
              if (atingiuLimite) {
                let espaco =''
                if (acc < qtdCaracterTela -1)
                 espaco = ' '.repeat(qtdCaracterTela - acc)
                arrnew[++indice] = '|| |'+acc+ espaco+'|';
                acc =""
            
              } else {
                acc += palavra+ " ";
            
              }
              
              return acc;
            
            
            },'');
            return arrnew.join(' ');
            };
            return createTimeline(porEmotion(text))
      })
      .catch((error) => {
        console.log('Saving failed: ', error);
      });
  }